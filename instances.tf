data "template_file" "user-data-start-audible-ec2" {
  template = "${file("user-data/start-audible-ec2.tpl")}"

  vars {
    last_fm_api_key = "${var.last-fm-api-key}"
  }
}

data "aws_ami" "audible" {
  most_recent = true
  owners = ["self"]
  filter {
    name = "tag:Application"
    values = ["audible"]
  }
}

resource "aws_launch_configuration" "web" {
  image_id                    = "${data.aws_ami.audible.id}"
  instance_type               = "t2.micro"
  security_groups             = ["${aws_security_group.basic_security_group.id}"]
  key_name                    = "anotherkey"
  iam_instance_profile        = "${aws_iam_instance_profile.ec2-profile.id}"
  user_data                   = "${data.template_file.user-data-start-audible-ec2.rendered}"
  associate_public_ip_address = true

  lifecycle {
    create_before_destroy = true
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = 8
  }
}

resource "aws_autoscaling_group" "web-autoscaling" {
  launch_configuration = "${aws_launch_configuration.web.id}"
  vpc_zone_identifier = ["${aws_subnet.sn1.id}", "${aws_subnet.sn2.id}", "${aws_subnet.sn3.id}"]
  load_balancers = ["${aws_elb.web-autoscaling-lb.id}"]
  min_size = 1
  max_size = 2

  tag {
    key = "Name"
    value = "web-autoscaling"
    propagate_at_launch = true
  }
}

resource "aws_elb" "web-autoscaling-lb" {
  name = "web-autoscaling-lb"
  security_groups = ["${aws_security_group.basic_security_group.id}"]
  subnets = ["${aws_subnet.sn1.id}", "${aws_subnet.sn2.id}", "${aws_subnet.sn3.id}"]

  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "${var.port}"
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 2
    interval = 30
    target = "HTTP:${var.port}/health"
  }
}

