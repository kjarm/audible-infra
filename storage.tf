resource "aws_s3_bucket" "ec2_to_s3" {
  bucket = "kacdab-workshop-ec2-s3"
  acl    = "private"

  tags {
    Name        = "EC2 to S3 bucket"
  }
}