terraform {
  backend "s3" {
    dynamodb_table = "audible-terraform-lock"
    bucket         = "audible-terraform"
    key            = "terraform.tfstate"
    region         = "eu-west-1"
  }
}
