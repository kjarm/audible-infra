variable "last-fm-api-key" {
  type = "string"
}

variable "port" {
  type = "string"
  default = 3000
}
