data "template_file" "s3-get-object-policy" {
  template = "${file("policies/s3-get-object.json")}"
   vars {
    s3_bucket = "${aws_s3_bucket.ec2_to_s3.arn}"
  }
}
 resource "aws_iam_role" "ec2-assume-role-policy" {
  name = "ec2-assume-role-policy"
  assume_role_policy = "${file("policies/ec2-assume-role.json")}"
}
 resource "aws_iam_policy" "s3-get-policy" {
  name   = "s3-get-role-policy"
  policy = "${data.template_file.s3-get-object-policy.rendered}"
}

resource "aws_iam_policy_attachment" "ec2-s3-attachment" {
  name       = "ec2-s3-attachment"
  roles      = ["${aws_iam_role.ec2-assume-role-policy.id}"]
  policy_arn = "${aws_iam_policy.s3-get-policy.arn}"
}
resource "aws_iam_instance_profile" "ec2-profile" {
  name  = "ec2-profile"
  role = "${aws_iam_role.ec2-assume-role-policy.id}"
}
