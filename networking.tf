resource "aws_vpc" "main" {
  provider   = "aws.kjarm"
  cidr_block = "192.168.0.0/16"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "sn1" {
  availability_zone = "eu-west-1a"
  cidr_block        = "192.168.1.0/24"
  provider          = "aws.kjarm"
  vpc_id            = "${aws_vpc.main.id}"
}

resource "aws_subnet" "sn2" {
  availability_zone = "eu-west-1b"
  cidr_block        = "192.168.2.0/24"
  provider          = "aws.kjarm"
  vpc_id            = "${aws_vpc.main.id}"
}

resource "aws_subnet" "sn3" {
  availability_zone = "eu-west-1c"
  cidr_block        = "192.168.3.0/24"
  provider          = "aws.kjarm"
  vpc_id            = "${aws_vpc.main.id}"
}

resource "aws_subnet" "sn4" {
  availability_zone = "eu-west-1a"
  cidr_block        = "192.168.4.0/24"
  provider          = "aws.kjarm"
  vpc_id            = "${aws_vpc.main.id}"
}

resource "aws_subnet" "sn5" {
  availability_zone = "eu-west-1b"
  cidr_block        = "192.168.5.0/24"
  provider          = "aws.kjarm"
  vpc_id            = "${aws_vpc.main.id}"
}

resource "aws_subnet" "sn6" {
  availability_zone = "eu-west-1c"
  cidr_block        = "192.168.6.0/24"
  provider          = "aws.kjarm"
  vpc_id            = "${aws_vpc.main.id}"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "main"
  }
}

resource "aws_route_table" "rt" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags {
    Name = "main"
  }
}

resource "aws_route_table_association" "sna1" {
  subnet_id      = "${aws_subnet.sn1.id}"
  route_table_id = "${aws_route_table.rt.id}"
}

resource "aws_route_table_association" "sna2" {
  subnet_id      = "${aws_subnet.sn2.id}"
  route_table_id = "${aws_route_table.rt.id}"
}

resource "aws_route_table_association" "sna3" {
  subnet_id      = "${aws_subnet.sn3.id}"
  route_table_id = "${aws_route_table.rt.id}"
}
