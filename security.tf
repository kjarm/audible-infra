resource "aws_security_group" "basic_security_group" {
  name        = "allow_all"
  description = "Allow all inbound traffic"
  vpc_id      = "${aws_vpc.main.id}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "allow_icmp" {
  type        = "ingress"
  from_port   = -1
  to_port     = -1
  protocol    = "icmp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.basic_security_group.id}"
}

resource "aws_security_group_rule" "allow_office_tcp" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["217.153.94.18/32", "87.207.93.8/32"]
  security_group_id = "${aws_security_group.basic_security_group.id}"
}

resource "aws_security_group_rule" "allow_http_3000" {
  type = "ingress"
  from_port = 3000
  to_port = 3000
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.basic_security_group.id}"
}

resource "aws_security_group_rule" "allow_http_80" {
  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.basic_security_group.id}"
}

resource "aws_security_group_rule" "allow_outside_world" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.basic_security_group.id}"
}

resource "aws_key_pair" "keypair" {
  key_name   = "mykey"
  public_key = "${file("/home/kjarmicki/.ssh/id_rsa.pub")}"
}
